---
duration: 40
presentation_url: null
room: PennTop South
slot: 2019-10-05 09:20:00-04:00
speakers:
- Piper Thunstrom
title: Accepting your successes
type: keynote
video_url: null
---

Like many people, Piper has struggled with accepting her competencies. It
was hard to ignore the many resounding failures in her life, until she
looked up one day, and realized she'd met all the markers of her childhood
dreams. Now what to do with "success"?
