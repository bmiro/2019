---
name: Manning
tier: media
site_url: https://www.manning.com/
logo: manning.png
twitter: ManningBooks
---
Manning is an independent publisher of computer books for software developers, engineers,
architects, system administrators, managers and all who are professionally involved with the
computer business. The books we publish cover a huge range of topics that the modern developer
needs; from languages and frameworks, to best practices for team leaders. 
