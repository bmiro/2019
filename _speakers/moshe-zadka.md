---
name: Moshe Zadka
talks:
- "A Time Traveler\u0027s Perspective on Software Developer Code of Ethics"
---
Moshe has been involved in the Linux community since 1998, helping in Linux
"installation parties". He has been programming Python since 1999, and has
contributed to the core Python interpreter. Moshe has been a DevOps/SRE
since before those terms existed, caring deeply about software reliability,
build reproducibility and other such things. He has worked in companies as
small as three people and as big as tens of thousands -- usually some place
around where software meets system administration.
